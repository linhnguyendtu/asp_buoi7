﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Demo7
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        String conn = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Study\ASPNET\Demo7\Demo7\App_Data\QLBH.mdf;Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;//Nếu như trang đã load thì lấy dùng luôn
            String sql1 = "select * from LoaiHang";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql1, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.DataList1.DataSource = dt;
                this.DataList1.DataBind();
            }
            catch(SqlException ex)
            {
                Response.Write(ex.Message);
            }

            
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }
    }
}