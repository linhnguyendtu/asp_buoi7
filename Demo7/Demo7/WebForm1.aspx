﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo7.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Demo7</title>
    <link href="StyleSheet1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="menu">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Home.aspx">Home</asp:HyperLink>
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Product.aspx">Product</asp:HyperLink>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/News.aspx">News</asp:HyperLink>
                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Contact.aspx">Contact</asp:HyperLink>
            </div>
            <div class="banner"></div>
            <div class="breadcrumb">
                <marquee onmouseover=stop() onmouseout=start()>Quảng Cáo</marquee>
            </div>
            <div class="sidebar">
                <h2>DANH MỤC</h2>
                <asp:LinkButton ID="LinkButton1" runat="server">Áo Quần</asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server">Giày Dép</asp:LinkButton>
                <asp:LinkButton ID="LinkButton3" runat="server">Phụ Kiện</asp:LinkButton>
                <asp:LinkButton ID="LinkButton4" runat="server">Chính sách bảo hành</asp:LinkButton>
            </div>
            <div class="content">
                Nội Dung
            </div>
            <footer>
                @CopyRight LinhNV </br>
                Địa chỉ: Đà Nẵng
            </footer>
        </div>
    </form>
</body>
</html>
